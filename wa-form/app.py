import time
import shutil
from whatsapp import WhatsApp
from openpyxl import load_workbook

# Only read (template)
book = load_workbook('data.xlsx')
sheet = book.active

def main():
	time.sleep(10)
	whatsapp = WhatsApp(10, session="mysession")
	while True:
		user = whatsapp.unread_usernames(scrolls=10)
		print(user)
		int_user = len(user)
		int_arr_user = int_user -1

		if (int_user==""):
			print("Kosong")
		else:	
			message = whatsapp.unread_messages(scrolls=10)
			print(message)
			int_msg = len(message)
			int_arr_msg = int_msg -1

			for i in range (0, int_user):
				# Get username
				print_user = user[int_arr_user]
				print(print_user)

				# Get message
				print_msg = message[int_arr_msg]
				text1 = print_msg.replace("Nama: ", "")
				if(print_msg==text1):
					text1 = print_msg.replace("Nama:", "")

				text2 = text1.replace(" Alamat: ", "#")
				if(text1==text2):
					text2 = text1.replace(" Alamat:", "#")

				text3 = text2.replace(" HP: ", "#")
				if(text2==text3):
					text3 = text2.replace(" HP:", "#")
				#print(text3)

				txt = Convert(text3)

				# Cetak list
				print(txt)

				# Hitung jumlah list
				jumlah = len(txt)
				print(jumlah)

				if(jumlah==3):
					nama = txt[0]
					alamat = txt[1]
					hp = txt[2]

					# Last column in column A
					colummn=sheet['A']
					last_data = len(colummn)

					# Save in Spreadsheet
					for row in range(last_data+1, last_data+2):
						sheet.cell(column=1, row=row, value="{0}".format(print_user))
						sheet.cell(column=2, row=row, value="{0}".format(nama))
						sheet.cell(column=3, row=row, value="{0}".format(alamat))
						sheet.cell(column=4, row=row, value="{0}".format(hp))
					book.save('temp/data-read.xlsx')  

					# Copy file to template
					shutil.copyfile('temp/data-read.xlsx', 'data.xlsx')

					# Send message if format is true
					cek = print_msg.startswith("Nama:")
					if(cek):
						print(whatsapp.send_message(print_user,"Terima kasih sudah mengisi formulir"))

				else:
					print("Tidak sesuai format")
					print(whatsapp.send_message(print_user,""))	
		#refresh
		whatsapp.refresh("TITIPAN")
		time.sleep(3)

# Python code to convert string to list 
def Convert(string): 
    li = list(string.split("#")) 
    return li 

if __name__ == '__main__':
    main()	